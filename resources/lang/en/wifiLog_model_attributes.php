<?php

return [

    /*
    |--------------------------------------------------------------------------
    | wifiLog Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'name' => 'Name',
    'surname' => 'Surname',
    'mail' => 'Mail',
    'telephone' => 'Phone',
    'address' => 'Address',
    'created_at' => 'Created',
    'updated_at' => 'Updated',
    'id' => 'ID'
];
