<?php

return [

    /*
    |--------------------------------------------------------------------------
    | wifiLog Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'name' => 'Nombre',
    'surname' => 'Apellido',
    'mail' => 'E-mail',
    'telephone' => 'Teléfono',
    'address' => 'Dirección',
    'created_at' => 'Creado',
    'macAddress' => 'MAC',
    'updated_at' => 'Actualizado',
    'id' => 'ID'
];
