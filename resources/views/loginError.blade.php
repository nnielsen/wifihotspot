@extends('layouts.app')

@section('content')
<div class="centerall">
    <div class="columns is-mobile">
  <div class="column is-full-tablet">
        <article class="message is-info" id="errors">
                <div class="message-header">
                    <p>Error</p>
                    <a href="{{route('login.options')}}">
                        <button class="delete" aria-label="delete" type="button" id="errors-close"></button>
                    </a>
                </div>
                <div class="message-body">
                 Intente nuevamente
                </div>
            </article>
            
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(()=>{
    $('.centerall').click(()=>
    {
        location.href = '{{route('login.options')}}';
    })
})
</script>
@endsection