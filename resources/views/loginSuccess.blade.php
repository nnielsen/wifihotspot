@extends('layouts.app')

@section('content')
<div class="centerall">
    <div class="columns is-mobile">
  <div class="column is-full-tablet">
        <article class="message is-info" id="errors">
                <div class="message-header">
                    <p>Autorizaci&oacute;n exitosa</p>
                </div>
                <div class="message-body">
                 <div class="has-text-centered">
                    <img src="{{asset('img/loading/loading.gif')}}">
                 </div>
                 <p>
                     Esper&aacute; un momento...
                 </p>

                </div>
            </article>
        </div>
    </div>
</div>

<iframe src="https://www.youtube.com/embed/connectionTest" style="display: none" id="iframe"></iframe>

@section('scripts')

<script type="text/javascript">
    var checkConnection = function(){
        console.info('Intentando conexion');
        var iframe = $("#iframe")[0];
        iframe.onload = connectionSuccess;
    }
    var retryOnFail = function()
    {
        console.error('Internet no disponible. Reintentando..')
        setTimeout(checkConnection, 3000);
    };
    var connectionSuccess = function()
    {
        location.href = 'http://google.com';
    }
    checkConnection();

</script>
@endsection