@extends('layouts.app')

@section('content')
<style type="text/css">
    .social-login-icon{
    border-radius: 3px 26px 23px 3px;
    position: absolute;
    max-width: 57px;
    max-height: 52px;
    float: left;
    left: 0;
    top: 0;
}
.fa.social-login-icon{
    background: #5d818cc4;
    padding: 15px;
}
section.login-option{
  padding-top: 1.4em;
  padding-bottom: 1.4em;
}
</style>
<div class="centerall">
    <div class="columns is-mobile">
        <div class="column is-full-tablet" style="min-width: 300px;">
        <div class="box">
            <div class="has-text-centered">
                <img src="{{asset('img/logo.png')}}" style="max-width: 290px;">
            </div>
        </div>
        <div class="has-text-centered">
            <h2 class="title has-text-white">Ingresar con</h2>
        </div>
        <section class="login-option">
            <div class="field">
                <a class="button is-large is-link is-inverted is-outlined is-fullwidth" href="{{route('redirect.to.google')}}">
                    <img class="social-login-icon" src="{{asset('img/social/google.png')}}">Google
                </a>    
            </div>
        </section>
        <section class="login-option">
            <div class="field">
                <a class="button is-large is-link is-inverted  is-outlined is-fullwidth" href="{{route('redirect.to.facebook')}}">
                    <img class="social-login-icon" src="{{asset('img/social/facebook.png')}}">Facebook
                </a>    
            </div>
        </section>
        <section class="login-option">
            <div class="field">
                <a class="button is-large is-link is-inverted  is-outlined is-fullwidth" href="{{route('login.form')}}">
                    <i class="fa fa-wpforms fa-3 social-login-icon" aria-hidden="true"></i>Formulario
                </a>    
            </div>
        </section>

    </div>
</div>

</div>
@endsection
@section('scripts')
@endsection