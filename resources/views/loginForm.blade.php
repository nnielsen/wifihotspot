@extends('layouts.app')

@section('content')
<div class="centerall">
  <div class="columns is-mobile">
    <div class="column is-full-tablet">
      {{Form::open(['route' => 'login.doLogin'])}}
      @csrf
      @if ($errors->any())
      <div id="errorModal" class="modal is-active">
        <div id="errorModalBackground" class="modal-background"></div>
        <div class="modal-content">
         <article class="message is-warning" id="errors">
          <div class="message-header">
            <p>Atenci&oacute;n</p>
            <button class="delete" aria-label="delete" type="button" id="errors-close"></button>
          </div>
          <div class="message-body">
            @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
            @endforeach
          </div>
        </article>
      </div>
    </div>

    @endif
    @foreach ($fields as $key)

    <div class="field">
      <p class="control has-icons-left">
        <input id="inp{{$key['name']}}" name="{{$key['name']}}" class="input is-large" type="text" placeholder="{{__('wifiLog_model_attributes.'.$key['name'])}}" value="{{ $errors->first($key['name'])?'': old($key['name']) }}" autocomplete="off">
        <span class="icon is-small is-left">
          <i class="fa {{$key['icon']}}"></i>
        </span>
      </p>

    </div>
    @endforeach
    <div class="field">
      <input type="submit" class="button is-fullwidth is-large is-info" />
    </div>
    <div class="field">
      <a class="button is-fullwidth is-large" href="{{route('login.showOptions')}}" />Volver</a>
    </div>
  </div>

  {{Form::close()}}
</div>
</div>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(()=>
  {
    var closeErrorModal = (event)=>{
      $("#errorModal").removeClass('is-active');
    };
    $("#errors-close").click(closeErrorModal);
    $("#errorModalBackground").click(closeErrorModal);


  });
</script>

@endsection