export default {
	metodosIngreso :[
	{
		name:'Facebook',
		img:'img/social/facebook.png',
		action:{
			href:'aver.com'
		}
	},{
		name:'Google',
		action:{
			href:'aver.com'
		},
		img:'img/social/google.png',
	},{
		name:'Formulario',
		icon:'wpforms',
		action:{
			click:'openFormModal'
		}
	}
	]
}
