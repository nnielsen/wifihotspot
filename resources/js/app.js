require('jquery');
window.Vue = require('vue');
const source = require('./source.js').default;

require('dotenv').config();

import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'

import Main from './components/Main.vue';


Vue.use(Buefy)
new Vue({
	el:'#app',
	data:source,
	created: function () {

  	},
  	render: h=>h(Main)
})
