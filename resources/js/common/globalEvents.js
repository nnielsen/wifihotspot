
export default{
	successfulConnection()
	{
		window.location.href = process.env.MIX_REDIRECT_TO
	},
	redirect(linkTo)
	{
		window.location.href = linkTo;
	}
}