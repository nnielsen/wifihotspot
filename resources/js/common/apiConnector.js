const axios = require('axios');
const routes = require('../../sync/apiRoutes.json');

export  default{
	getRoutes()
	{
		return routes;
	},
	dispatchAction(routeCommand,payload={}){
		let action = routes[routeCommand];
		switch( action.methods[0].toLowerCase() )
		{
			case 'get':
			return axios.get(action.uri)
			case 'post':
			return axios.post(action.uri,payload)
		}
	}
}