<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        foreach (\Config::get('constants.login_types') as $key) 
        {
             DB::table('login_type')->insert(
                array(
                    'name' => $key
                )
            );
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_type');
    }
}
