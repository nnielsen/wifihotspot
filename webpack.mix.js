const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   //.sass('resources/sass/app.scss', 'public/css')
   .js('resources/js/adminApp.js', 'public/js')
   .copy('resources/img/social/*.*','public/img/social')
   .copy('resources/img/loading/loading.*','public/img/loading')
   .copy('node_modules/jquery/dist/jquery.min.js','public/js')

mix.webpackConfig({node: {
   fs: "empty"
}});