<?php

use Illuminate\Http\Request;

Route::get('login/options', 'hotspotLogin@getOptions')->name('login.options');
Route::post('login/create','hotspotLogin@loginWithApi')->name('login.create');
