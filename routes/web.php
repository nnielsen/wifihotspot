<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| FRONT END framework :    BLADE / VUE
|
*/
if(($frontEnd = env('APP_FRONT_END','blade'))=='vue')
{
	Route::get('/','hotspotLogin@initApp')->name('init.app');
	Route::get('guest/s/{guestSite}/', 'hotspotLogin@storeAndInit');
}else
{
	Route::get('/','hotspotLogin@showOptions')->name('login.showOptions');
	Route::get('guest/s/{guestSite}/', 'hotspotLogin@showOptions');
	Route::post('/dologin', 'hotspotLogin@loginWithSuccessView')->name('login.doLogin');
	Route::get('login/form', 'hotspotLogin@showForm')->name('login.form');
	Route::view('login/error', 'loginError');
	Route::view('login/success', 'loginSuccess');
}

/* Reportes */
Route::get('/reporte', 'LoginReport@getIndex');
Route::get('/dataTablaReporte','LoginReport@tableViewData')->name('data.reporte');



Route::prefix('auth')->group(function () {
    /* Google socialite */
	Route::get('google/redirect', 'Auth\LoginController@redirectToGoogle')->name('redirect.to.google');
	Route::get('google/callback', 'Auth\LoginController@handleGoogleCallback')->name('handle.google.callback');
	/* Facebook socialite */
	Route::get('facebook/redirect', 'Auth\LoginController@redirectToFacebook')->name('redirect.to.facebook');
	Route::get('facebook/callback', 'Auth\LoginController@handleFacebookCallback')->name('handle.facebook.callback');

});


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

