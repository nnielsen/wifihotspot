<?php

namespace App;

use App\customModel;

class wifiLog extends customModel
{
     protected $table = 'logs';
     protected $fillable = ['name','surname','mail','address','telephone','macAddress','login_type','apSite'];
     public function type()
     {
     	return $this->hasOne('App\loginType');
     }
     public function getAttributesCustom()
     {
          return  ['name','surname','mail','address','telephone','macAddress','created_at'];
     }
     public function getLoginTypeAttribute()
     {
     	return $this->type;
     }
     public function getLoginTypeNameAttribute()
     {
     	return $this->type->name;
     }
     public function getLoginTypeIdAttribute()
     {
     	return $this->type->id;
     }
}
