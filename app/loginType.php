<?php

namespace App;

use App\customModel;

class loginType extends customModel
{
    protected $table = 'login_type';
    public static function byType($type)
    {
    	return static::where('name', $type)->first();
    }
}
