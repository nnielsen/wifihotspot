<?php

namespace App;

use \UniFi_API\Client as unifiClient;

class unifiConnection
{
    public static function registerGuest($wifiLog,$note = 'Autorizacion desde Laravel-WifiHotspot')
    {
        $unifi_connection = new unifiClient
        (
        	env('UNIFI_USER'), 
	        env('UNIFI_PASSWORD'),
	        env('UNIFI_SERVER'),
	        $wifiLog->apSite,
	       	env('UNIFI_CONTROLLER_VERSION'), 
	        true
	    );
		$unifi_connection->set_ssl_verify_host(false);
		$unifi_connection->set_ssl_verify_peer(false);

		$unifi_connection->set_debug(env('UNIFI_CLIENT_DEBUG') == 'true');

		$loginResult 				= [];
		$loginResult['login']       = $unifi_connection->login();
		$loginResult['list_alarms'] = $unifi_connection->list_alarms();

		$auth_result  = $unifi_connection->authorize_guest($wifiLog->macAddress, env('UNIFI_AUTH_DURATION'));
		$getid_result = $unifi_connection->stat_client($wifiLog->macAddress);
		//$user_id      = $getid_result[0]->_id;
		//$note_result  = $unifi_connection->set_sta_note($user_id, $note);
		return [
				'unifi_connection' => $unifi_connection,
				'loginResult' => $loginResult,
				'auth_result'=> $auth_result,
				'payload' => [
					'clientMac' => $wifiLog->macAddress
					]
				];
    }
}
