<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Requests\hotspotLoginPost;
use App\wifiLog;
use App\unifiConnection;
use App\Jobs\SendWhatsApp as SendWhatsAppJob;
use App\WhatsAppMessage;
use App\loginType;
use App\loginOptions;

class hotspotLogin extends Controller
{
    public function login(hotspotLoginPost $request)
    {  
        $request = $request->all();
        $request['login_type'] = loginType::byType('form')->id;
        return $this->createLog($request);
    }
    public function loginWithSuccessView(hotspotLoginPost $request)
    {  
        $request = $request->all();
        $request['login_type'] = loginType::byType('form')->id;
        $this->createLog($request);
        return view('loginSuccess');
    }
    public function loginWithApi(hotspotLoginPost $request)
    {
        return $this->login($request);
    }
    public function createLog($request) 
    {
        $wifiLog = new wifiLog;

        $fillable = ['name','surname','mail','address','telephone','macAddress'];
        
        foreach ($fillable as $field) 
        {
            if(isset($request[$field]))
            {
                $wifiLog->$field = $request[$field];    
            }
        }
        unset($field);

        $wifiLog->login_type_fk = $request['login_type'];
        
        $wifiLog->macAddress = session('macAddress');
        $wifiLog->apSite = session('apSite');

        if(empty($wifiLog->macAddress))
        {
            //throw new \Exception("Mac Address es requerida");
        }

        $wifiLog->save();

        try
        {
            if(env('APP_MOCK_UNIFI','false')=='true')
            {
                $payload = '{"mocked_unifi_connection":{},"loginResult":{"login":true,"list_alarms":[{"_id":"5cf8a11b8e89bd044a0b7fcf","archived":false,"key":"EVT_AP_Lost_Contact","ap":"fc:ec:da:89:f9:88","ap_name":"Unknown[fc:ec:da:89:f9:88]","subsystem":"wlan","site_id":"5cf879858e89bd08871ec0a9","time":1559798043312,"datetime":"2019-06-06T05:14:03Z","msg":"AP[fc:ec:da:89:f9:88] was disconnected"}]},"auth_result":true,"payload":{"clientMac":null}}';
            }else{
                $payload = $this->registerGuest($wifiLog);    
            }
            
            return $payload;
        }catch(\Exception $e)
        {
             if(env('APP_ENV')=='local')
             {
                throw $e;
             }
             abort(500);
        }
    }
    public function registerGuest($wifiLog)
    {
        $this->connection = unifiConnection::registerGuest($wifiLog);
        return $this->connection;
    }
    private function storeMacs(Request $request)
    {
        session([
            'macAddress'=>$request->id,
            'apSite'=> $request->route('guestSite')
        ]);
    }
    public function showOptions(Request $request)
    {
        $this->storeMacs($request);
    	return view('loginOptions');
    }
    public function storeAndInit(Request $request)
    {
        $this->storeMacs($request);
        return redirect()->route('init.app');
    }
    public function initApp(Request $request)
    {
        return view('vueApp');
    }
    public function getOptions(Request $request)
    {
        $this->storeMacs($request);
        $options = new loginOptions();
        return $options->list;
    }
    public function showForm()
    {
        $fields = [
            ['name'=>'name','icon'=>'fa-user'],
            ['name'=>'surname','icon'=>'fa-group'],
            ['name'=>'mail','icon'=>'fa-envelope'],
            ['name'=>'address','icon'=>'fa-home'],
            ['name'=>'telephone','icon'=>'fa-phone'],
        ];

        return view('loginForm',['fields'=>$fields]);
    }
    protected function sendWhatsApp($to)
    {
        SendWhatsAppJob::dispatch(new WhatsAppMessage($to));
    }

}
