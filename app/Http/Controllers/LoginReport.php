<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\loginsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\wifiLog;

class LoginReport extends Controller
{
    protected function tableViewData(Request $request)
    {
    	return DataTables::eloquent(wifiLog::query())->make(true);
    }

    /**
	 * Displays datatables front end view
	 *
	 * @return \Illuminate\View\View
	 */
	public function getIndex(Request $request)
	{
        if($request->query('format'))
        {
            return $this->all($request);
        }
        $wifiLog = new wifiLog;
        $fields = $wifiLog->getAttributesCustom();
	    return view('report.index',["fields"=>$fields]);
	}
    public function all(Request $request)
    {
    	$loginsExport = new loginsExport;
    	switch ($request->query('format', '')) {
    		case 'csv':
    			return Excel::download($loginsExport, 'logins.csv');
    		case 'html':
    			return Excel::download($loginsExport, 'logins.html');
    		case 'xlsx':
    			return Excel::download($loginsExport, 'logins.xlsx');
    		case 'xls':
    			return Excel::download($loginsExport, 'logins.xls');
    		case 'json':
    		default:
    			return response()->json($loginsExport->collection());
    			break;
    	}
    }
}
