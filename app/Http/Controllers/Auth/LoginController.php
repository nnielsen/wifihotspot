<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\wifiLog;
use App\loginType;
use App\Http\Controllers\hotspotLogin;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
      * Redirect the user to the Google authentication page.
      *
      * @return \Illuminate\Http\Response
      */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleGoogleCallback()
    {
        try {
            $googlePayload = Socialite::driver('google')->user();
            $user = $googlePayload->user;
        } catch (\Exception $e) {
            return redirect('/login');
        }

        $wifiLog = app('App\Http\Controllers\hotspotLogin')->createLog([
            'name' => $user['given_name'],
            'surname' => $user['family_name'],
            'mail' => $user['email'],
            'address' => '',
            'login_type'=> loginType::byType('google')->id
        ]);

        return view('loginSuccess', ['REDIRECT_DELAY' => env('REDIRECT_DELAY')]);
        
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        try {
            $facebookPayload = Socialite::driver('facebook')->user();
            $user = $facebookPayload->user;
            $names = explode(' ', $user['name']);
            $surname = array_pop($names);
            $user['name'] = implode(' ', $names);
            $user['surname'] = $surname;
        } catch (Exception $e) {
            return redirect('/login');
        }
        $wifiLog = app('App\Http\Controllers\hotspotLogin')->createLog([
            'name' => $user['name'],
            'surname' => $user['surname'],
            'mail' => $user['email'],
            'address' => '',
            'login_type'=> loginType::byType('facebook')->id
        ]);

        return view('loginSuccess', ['REDIRECT_DELAY' => env('REDIRECT_DELAY')]);
    }
}
