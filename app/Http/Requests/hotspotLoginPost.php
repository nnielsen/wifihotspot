<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class hotspotLoginPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|max:255',
            'mail' => 'sometimes|email|required|max:255',
            'address' => 'sometimes|max:255',
            'surname' => 'sometimes|max:255',
            'telephone' => 'sometimes|phone|max:20',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nombre es requerido',
            'name.max' => 'Nombre debe ser menor a 255 caracteres',
            'mail.required'  => 'Mail es requerido',
            'mail.max' => 'Mail debe ser menor a 255 caracteres',
            'mail.email'  => 'Ingrese un e-mail válido',
            'mail.unique'  => 'Mail existente',
            'address.required'  => 'Dirección es requerido',
            'address.max' => 'Dirección debe ser menor a 255 caracteres',
            'surname.required'  => 'Apellido es requerido',
            'surname.max' => 'Apellido debe ser menor a 255 caracteres',
            'telephone.max' => 'Teléfono debe ser menor a 20 caracteres',
            'telephone.required' => 'Teléfono es requerido',
            'telephone.phone' => 'Teléfono es inválido'
        ];
    }
}
