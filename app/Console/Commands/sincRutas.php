<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;

class sincRutas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sinc-rutas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exportar rutas de api a mapeo de rutas de cliente JS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routeCollection = Route::getRoutes();
        $routes = [];
        foreach ($routeCollection as $route) 
        {
            if(isset($route->getAction()['as']))
            {
                 $routes[$route->getAction()['as']] = [
                    'uri'=> $route->uri,
                    'methods'=> $route->methods
                ];
            }
        }
        $handle = fopen(implode(DIRECTORY_SEPARATOR, [base_path(),'resources','sync','']) . 'apiRoutes.json' , 'w');
        fwrite($handle, json_encode($routes));
        fclose($handle);
        return 0;
    }
}
