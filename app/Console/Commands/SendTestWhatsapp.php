<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WhatsAppSenderFacade as sender;

class SendTestWhatsapp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'whatsapp:sendTest {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test WhatsApp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(sender $sender)
    {
        parent::__construct();

        $this->sender = $sender;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sender->sendMessage($this->argument('to'));
    }
}
