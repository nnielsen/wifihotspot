<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class loginOptions extends Model
{
	protected $_options;
	public function __construct()
	{
		$this->_options = [
			/*'google'=>[
				'name'=>'Google',
				'img'=> asset('img/social/google.png'),
				'linkTo'=> route('redirect.to.google')
			],
			'fb'=>[
				'name'=>'Facebook',
				'img'=> asset('img/social/facebook.png'),
				'linkTo'=> route('redirect.to.facebook')
			],*/
			'form'=>[
				'name'=>'Mail',
				'icon'=> 'fa-envelope',
				'action'=> 'inputMail'
			]
		];
	}
    public function getListAttribute()
    { 
    	return $this->_options;
    }
}
