<?php

namespace App\Exports;

use App\wifiLog;
use Maatwebsite\Excel\Concerns\FromCollection;

class loginsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return wifiLog::all();
    }
}
