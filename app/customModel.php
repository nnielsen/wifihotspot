<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class customModel extends Model
{
    public function getAttributesCustom()
    {
        if(empty($this->columns))
        {
            $this->columns = Schema::getColumnListing($this->getTable());    
        }
        return $this->columns;
    }
}
